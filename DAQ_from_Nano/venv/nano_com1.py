import serial
import time
import pandas as pd
import datetime
import matplotlib.pyplot as plt

arduino_data = serial.Serial('/dev/ttyUSB0', 9600)
nano1 = {'Date/ Time': [], 'Pressure [psi]': [], 'Temperature [Celsius]': []}
start_date = datetime.datetime.today().replace(microsecond=0)
print(start_date)
# receiving data from the nana and storing it in a dictionary
while True:
    if arduino_data.inWaiting() > 0:
        date = datetime.datetime.today().replace(microsecond=0)
        nano1['Pressure [psi]'].append(arduino_data.readline())
        nano1['Temperature [Celsius]'].append(arduino_data.readline())
        nano1['Date/ Time'].append(date.strftime("%d-%B-%Y  %H:%M:%S"))
# saving it to the csv
    if (len(nano1['Pressure [psi]']) % 5) == 4:
        csv1 = pd.DataFrame(nano1, columns=['Date/ Time', 'Pressure [psi]', 'Temperature [Celsius]'])
        csv1.to_csv('CSVs/' + str(date) + ".csv", index=False)
        print("Made a file!")
        print(nano1)
        time.sleep(1)
# plotting the data
        t = nano1['Date/ Time'][-50:]
        pressure = nano1['Pressure [psi]'][-50:]
        temp = nano1['Temperature [Celsius]'][-50:]
        print(t)
        print(pressure)

        fig, ax1 = plt.subplots()

        color = 'tab:red'
        ax1.set_xlabel('Time')
        plt.xticks(rotation=90)
        ax1.set_ylabel('Pressure [psi]', color=color)
        ax1.plot(t, pressure, color=color)
        ax1.tick_params(axis='y', labelcolor=color)

        ax2 = ax1.twinx()

        color = 'tab:blue'
        ax2.set_ylabel('Temperature [Celsius]', color=color)
        ax2.plot(t, temp, color=color)
        ax2.tick_params(axis='y', labelcolor=color)

        fig.tight_layout()
        plt.show()
        plt.savefig('Plots/' + str(date)+'.png')

    if len(nano1['Pressure [psi]']) > 60:
        break
